//
//  HomeViewController.m
//  HWRM
//
//  Created by delin.meng on 2020/10/23.
//

#import "HomeViewController.h"
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
//#import <ReactiveObjC.h>
#import "CalendarManager.h"

@interface HomeViewController ()

@property (strong, nonatomic, readwrite) CalendarManager *manager;

@end

@implementation HomeViewController

- (void)viewDidLoad {
  
  [super viewDidLoad];

  NSLog(@"%@",@"设备ID");
  
//  RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) { //1
//      [subscriber sendCompleted];
//      return nil;
//  }];
//
  
//  RACSubject *subject = [RACSubject subject];
//  [subject.rac_willDeallocSignal subscribeCompleted:^{
//    NSLog(@"subject dealloc");
//  }];
//  [subject subscribeNext:^(id  _Nullable x) {
//  }];
//
//  [subject sendNext:@1];

  
  self.manager = [[CalendarManager alloc] init];
//  RACSignal *signal = [RACObserve(self.manager, name) distinctUntilChanged];
//  [[signal filter:^BOOL(NSString  *newName) {
//    return YES; //[newName hasPrefix:@"j"];
//  }] subscribeNext:^(NSString  *x) {
//
//    NSLog(@" 阈值触发 %@", x);
//
//  }];

  
  // RAC(self.manager, createenabled)  底层实现是创建 target 关联属性
//  RAC(self.manager, createEnabled) = [[RACSignal combineLatest:@[
//    RACObserve(self.manager, password),
//    RACObserve(self.manager, passwordConfirmation)
//  ] reduce:^(NSString *password, NSString *passwordConfirm) {
//    return @( [passwordConfirm isEqualToString:password]);
//  }] subscribeNext:^(id  _Nullable x) {
//    NSLog(@" createenabled %@", createenabled)
//  }] ;
  
  UIButton *bt = [[UIButton alloc] init];
//  bt.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
//    NSLog(@"button was pressed!");
//    return [RACSignal empty];
//
//  }];
  [bt setTitle:@"设置" forState:UIControlStateNormal];
  bt.frame = CGRectMake(0, 0, 100, 100);
  [self.view addSubview:bt];
  
  
//  UITextField *usernameTextField = [[UITextField alloc ] init];
//  usernameTextField.rac_newTextChannel;
  
  
  
}

#pragma RCTBridgeDelegate
- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
  
}

- (IBAction)createAction:(UIButton *)sender {
  
  self.manager.name = @"j:title";
  
  
  
  
  
  
  
  
  
  
}



- (IBAction)submitAction:(UIButton *)sender {
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:nil];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"HWRM"
                                            initialProperties:nil];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
  UIViewController *RCTVC = [UIViewController new];
  [RCTVC.navigationController setNavigationBarHidden:YES];
  RCTVC.view = rootView;
  [self.navigationController setNavigationBarHidden:YES];
  [self.navigationController pushViewController:RCTVC animated:true];
}


@end
