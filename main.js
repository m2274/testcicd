import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import React, { Component } from 'react';

import SeatPage from './src/Pages/SeatPage/SeatPage'
import Home from './src/Pages/HomePage/Home'
import Index from './src/Pages/SearchPage/index'
import AinmationPage from "./src/Pages/AnimationPage/index";

const Stack = createStackNavigator();

export default class Entrance extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator>
                    {this.configMoudle().map((item,index) => {
                       return <Stack.Screen name={item?.name} component={ item?.component } />
                    })}
                </Stack.Navigator>
            </NavigationContainer>
        );
    }

    configMoudle = () => {
        return [
            {
                component:Home,
                name:'Home',
            },
            {
                component:SeatPage,
                name:'SeatPage',
            },
            {
                component:Index,
                name:'Index',
            },
            {
                component:AinmationPage,
                name:'AinmationPage',
            }
        ];
    }

}

