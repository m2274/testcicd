/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
  NativeModules,
  TouchableOpacity,
} from 'react-native';

import { Colors } from 'react-native/Libraries/NewAppScreen';

import React from 'react';
import PropTypes from 'prop-types';
import ComLineView from '../../common/views/ComLineView'

const Logo = {
  uri: 'https://reactnative.dev/img/tiny_logo.png',
  width: 30,
  height: 30
};

const Pages = [
  {
    page: 'SeatPage',
    name: ''
  },
  {
    page: 'Index',
    name: ''
  },
  {
    page:'AinmationPage',
    name:''
  }
];


export default class Home extends React.Component {

  static propTypes = {
    callBack: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);
  }

  // Action
  _handleNativeMethod = () => {
    const CalendarManager = NativeModules.CalendarManager;
    CalendarManager.addEvent(
      'Birthday Party',
      'four privet derive surrey'
    );
  }

  _handleSelected = (item) => {
    this.props.navigation.navigate(item.page)
  }

  _items = () => {
    const views = [];
    Pages.forEach(element => {
      views.push(this._renderItem(element));
    });
    return views;
  }

  _renderItem = (item) => {

    // source={require('../../asset/enlarge.png')

    return (
      <View style={styles.bgv}>
        <TouchableOpacity onPress={() => {
          this._handleSelected(item)
        }}>
          <View style={styles.contet}>
            <Text style={styles.Title}>{item.page + item.name}</Text>
            <Image source={Logo} />
          </View>
          <ComLineView></ComLineView>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView style={styles.scrollviewType}>
            {this._items()}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }

}

const styles = StyleSheet.create({

  bg: {
    backgroundColor: '#ffffff'
  },

  scrollviewType: {
    backgroundColor: "#ffffff",
  },

  bgv: {
    flex: 1,
    paddingLeft: 16,
    paddingRight: 16
  },

  contet: {
    flex:1,
    flexDirection:'row',
    backgroundColor: '#ffffff',
    justifyContent: 'space-between',
    alignItems:'center',
  },

  Title: {
    flex: 1,
    fontSize: 18,
    fontWeight: '600',
    color: Colors.black,
    marginTop: 16,
    marginBottom: 16,
  }

});