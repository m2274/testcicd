import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from "react-native";

import TrainBottomModalView from './view/TrainBottomModalView'

export default class SeatPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false
        }
    }

    render() {
        const { modalVisible } = this.state
        return (
            <View style={styles.centeredView}>
                
                <TrainBottomModalView
                    animationType='slide'
                    transparent={true}
                    modalVisible={modalVisible}
                    drawStyle="bottom"
                    presentationStyle='overFullScreen'
                    onRequestClose={() => {
                        this.setState({
                            modalVisible: false
                        })
                    }}
                    doneHandle={(item) => { // seleced item call back 
                        this.setState({
                            modalVisible: false
                        })
                    }}
                >
                </TrainBottomModalView>


                <TouchableHighlight
                    style={styles.openButton}
                    onPress={() => {
                        this.setState({
                            modalVisible: true
                        })
                    }}
                >
                    <Text>Show Modal</Text>
                </TouchableHighlight>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

    modalView: {
        flex: 1,
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: 'stretch',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    
});

