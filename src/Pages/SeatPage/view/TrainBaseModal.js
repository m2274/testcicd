import React, { Component } from 'react'
import PropTypes from 'prop-types';
import {
    Modal,
    StyleSheet,
    View
} from "react-native";

// 因JS对于面向接口编程不太友好，所以只能用继承实现某些功能

export default class TrainBaseModal extends Component {

    static propsType = {
        animationType: 'none' | 'slide' | 'fade',
        transparent: PropTypes.bool,
        modalVisible: PropTypes.bool,
        onRequestClose: PropTypes.func,
        presentationStyle:PropTypes.string, // enum('fullScreen', 'pageSheet', 'formSheet', 'overFullScreen')
        drawStyle: PropTypes.oneOfType['top', 'center', 'bottom']
    };

    constructor(props) {
        super(props);
    }

    renderMainContent() {
        return (<View></View>);
    }

    // 重写时运用
    renderContent = (drawStyle) => {
        switch (drawStyle) {
            case "top":
                return this.renderTopContent();
            case "center":
                return this.renderCenterContent();
            case "bottom":
                return this.renderBottomContent();
            default:
                break;
        }
    }

    // topView 重写自定义样式
    renderTopContent() {
        return (<View></View>);
    }

    // centerView 重写自定义样式
    renderCenterContent() {
        return (<View></View>);
    }

    // bottomView 重写自定义样式
    renderBottomContent() {
        return (<View></View>);
    }

    render() {
        const {
            animationType,
            transparent,
            modalVisible,
            onRequestClose,
            presentationStyle,
            drawStyle
        } = this.props

        return (
            modalVisible &&
            <View style={styles.centeredView}>
                <Modal
                    animationType={animationType}
                    transparent={transparent}
                    visible={modalVisible}
                    presentationStyle={presentationStyle}
                    onRequestClose={() => {
                        onRequestClose && onRequestClose();
                    }}
                >
                    <View style={styles.bgContent}>
                        {this.renderMainContent()}
                        {this.renderContent(drawStyle)}
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    centeredView: {
        flex:1,
        // position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.8)'
    },

    bgContent: {
        flex: 1,
        flexDirection: 'column-reverse'
    }


});





