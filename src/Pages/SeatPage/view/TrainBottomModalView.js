import React, { Component } from 'react'
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    Image,
    Button,
    TouchableOpacity
} from 'react-native'

import TrainBaseModal from './TrainBaseModal'
import ComLineView from '../../../common/views/ComLineView'
import PropTypes from 'prop-types';

export default class TrainBottomModalView extends TrainBaseModal {

    static propTypes = {
        doneHandle: PropTypes.func
    }

    constructor(props) {
        super(props);
        this.state = {
            selectedItem: null
        }
    }

    // done 
    doneAction = () => {
        this.props.doneHandle && this.props.doneHandle(this.state.selectedItem)
    }

    // close 
    closeAction = () => {
        this.props.onRequestClose && this.props.onRequestClose()
    }


    // bottomView
    renderBottomContent() {
        return (
            <View style={styles.bottomBGContent}>
                <View style={styles.bottomContentView}>
                    <Button onPress={this.closeAction}  title="Close"></Button>
                    <Text style={styles.bottomContentView_centerText}> Select Coach </Text>
                    <Button onPress={this.doneAction} style={styles.bottomContentView_rightText} title="Done"></Button>
                </View>
                <ComLineView></ComLineView>
            </View>
        );
    }

    // main view 
    renderMainContent() {
        const datas = ["crup", "crup", "crup", "crup", "crup", "crup"]
        return (
            <View style={styles.mainContent}>
                <FlatList
                    data={datas}
                    keyExtractor={index => index}
                    renderItem={this.renderItem} />
            </View>);
    }

    // render item 
    renderItem = (item) => {
        return (
            <TouchableOpacity onPress={ () => {
                this.setState = {
                    selectedItem:item
                }
            }}>
                <View style={styles.item}>
                    <Image source={require('../../../asset/enlarge.png')} />
                    <Text style={styles.item_coach}> Coach 4  </Text>
                    <Text style={styles.item_coach}> left 50 / total 56 seats </Text>
                </View>
                <ComLineView></ComLineView>
            </TouchableOpacity>
        );
    }

}

const styles = StyleSheet.create({

    // main 
    mainContent: {
        height: '64%',
        backgroundColor: '#ffffff'
    },

    // item  
    item: {
        flex: 1,
        paddingTop:16,
        paddingBottom:16,
        paddingLeft:16,
        paddingRight:16,
        flexDirection: 'row',
        justifyContent:'flex-start',
        alignItems:'center'
    },

    item_icon: {
        width:24,
        height:24,
    },

    item_coach: {
        paddingLeft:20,
    },

    item_seat: {
        paddingLeft:20,
        flexWrap:'wrap'
    },

    // Bottom 
    bottomBGContent: {
        height: '6%',
        backgroundColor: '#ffffff'
    },

    bottomContentView: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 16,
        paddingRight: 16,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    bottomContentView_image: {
        height: 30,
        width: 30
    },

    // 字体颜色
    bottomContentView_centerText: {


    },

    // 字体
    bottomContentView_rightText: {


    }

});