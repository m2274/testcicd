import React, { Component } from 'react'
import { View, Text, TouchableOpacity , KeyboardAvoidingView ,TextInput, Button , } from "react-native"
import { connect } from 'react-redux'
import HeaderView from './Views/HeaderView'
import AnimationView from './Views/AnimationView'


class SearchPage extends Component {

    constructor(props) {
        super(props);
        this.props.initData()
    }
    render() {
        const { headTitle, showCurrentPage, stations, isShowCurrentPage } = this.props;
        return (
            // <View>
            //     <TouchableOpacity onPress={() => {
            //         isShowCurrentPage(!showCurrentPage)
            //     }}>
            //         <Text>点击触发</Text>
            //     </TouchableOpacity>
            //     { showCurrentPage && <Text> {headTitle + stations.name + stations.age} </Text> }

            //     <HeaderView />
            // </View>


            <View>

                <AnimationView></AnimationView>

            </View>

        );
    }
}

// 获取需要的数据（only read  from  dif model）
const mapState = (state) => ({
    headTitle: state?.SearchPageModel?.headTitle,
    showCurrentPage: state?.SearchPageModel?.showCurrentPage,
    stations:state?.SearchPageModel?.stations
})


// 根据需求设置数据 ( write current model property )  或者操作其他一些操作
const mapDispatch = ({ SearchPageModel }) => ({

    initData: (value) => { SearchPageModel?.initData(value) },

    // 是带参数的方法 
    isShowCurrentPage: (value) => {
        SearchPageModel?.setState({
            showCurrentPage: value
        })
    }
})

export default connect(mapState, mapDispatch)(SearchPage)