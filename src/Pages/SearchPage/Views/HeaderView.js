
import React, { memo } from 'react'
import { PureComponent } from 'react'
import { View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';

const Nope = () => { }
class HeaderView extends PureComponent {

    constructor(props) {
        super(props);
    }

    render() {
        const { fetchHeaderData } = this.props
        return (
            <View style={ { flexDirection:'column' } }>
                <Text style={{ color: '#0809ED' }}> {this.props.headTitle} </Text>
                <TouchableOpacity onPress={() => {
                    fetchHeaderData("我是headerview")
                }}>
                    <Text> tap </Text>
                </TouchableOpacity>

            </View>
        );
    }
}


const mapState = (state) => ({

    headTitle: state?.SearchPageModel?.headTitle

})


const mapDispatch = ({ SearchPageModel }) => ({

    fetchHeaderData: (value) => { SearchPageModel?.fetchHeaderData(value) },

    // changeData: (value) => { SearchPageModel?.setState({
    //     headTitle:value
    // }) }

})

export default connect(mapState, mapDispatch)(HeaderView);




