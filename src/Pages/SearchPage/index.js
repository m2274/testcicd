import React, { Component } from 'react'
import {createStore} from './store'
import { Provider } from 'react-redux'
import SearchPage from "./page"

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.store = createStore();
    }

    render() {
        return(
            <Provider store={this.store}>
                <SearchPage {...this.props} />
            </Provider>
        );
    }
}