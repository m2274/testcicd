

import { init } from '@rematch/core'
import SearchPageModel from './model'

let store = init({
    models:{
        SearchPageModel
    }
})


const createStore = () => {
    const newStore = init({
        models:{
            SearchPageModel
        }
    })
    store = newStore
    return newStore
}

export {
    store,
    createStore,
};


