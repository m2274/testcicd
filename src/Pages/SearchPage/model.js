
import { initData , fetchHeaderData } from './action'

const initialState = {
    headTitle:"",
    showCurrentPage:false,
    stations:{
        name:null,
        age:null
    }
}

const SearchPageModel = {

    state: initialState,
    reducers: {
        setState(state, payload) {
            return {
                ...state,
                ...payload
            }
        },

        clear(state, playload) {
            return initState;
        }

    },

    effects: (dispatch) => ({

        async initData(params,rootState) {
            initData(rootState, dispatch, params)
        },

        async fetchHeaderData(params, rootState) {
            fetchHeaderData(params, rootState, dispatch)
        }
        
    })
}


export default SearchPageModel
