import React, { useState, useEffect, useRef, Component } from 'react'
import { FlatList, RefreshControl, ScrollView, Text, View } from 'react-native'
import PropTypes from 'prop-types';

export default class DataListView extends Component {

    static propTypes = {
        subIsScroll: PropTypes.bool,
        callBack: PropTypes.func
    }

    constructor(props) {
        super(props)
        this.state = {
            subIsScroll: this.props.subIsScroll
        }
        this.beginX = 0;
        this.beginY = 0;
    }

    componentDidMount

    

    getDatas = () => {
        let ds = [];
        for (let i = 0; i < 100; i++) {
            ds.push(`第${i}行`)
        }
        return ds
    }

    render() {

        const { callBack } = this.props
        const { subIsScroll } = this.state
        let ds = [];
        for (let i = 0; i < 100; i++) {
            ds.push(`第${i}行`)
        }
        return <FlatList

            onStartShouldSetResponder={ () => {
                return !subIsScroll
            }}
                // onScroll={(event) => {
                //     console.log('[ event ] >', event.nativeEvent.contentOffset.y)
                //     // 下拉
                //     if (event.nativeEvent?.contentOffset.y <=1) {
                //         this.setState({
                //             subIsScroll: false
                //         })
                //         // callBack && callBack(true)
                //     // 上提    
                //     }else{
                //         this.setState({ subIsScroll: true })
                //         // callBack && callBack(false)
                //     }
                // }}

                // 现在要开始响应触摸事件了。这也是需要做高亮的时候，使用户知道他到底点到了哪里。
                onResponderGrant={(event) => {
                    let obj = event.nativeEvent;
                    this.beginX = parseInt(obj.locationX);
                    this.beginY = parseInt(obj.locationY);
                }}

                // 手指移动后，释放
                onResponderRelease={(event) => {
                    let obj = event.nativeEvent;
                    let endX = parseInt(obj.locationX);
                    let endY = parseInt(obj.locationY);
                    let result = Math.abs(endX - this.beginX) > Math.abs(endY - this.beginY)
                    console.log('[ DataListView event ] >', this.beginX, this.beginY, endX, endY)
                    if (result) {
                        console.log('横向移动之后，判断是做正负');
                        if (endX > this.beginX) {
                            console.log('向右');
                        } else {
                            console.log('向左');
                        }
                    } else {
                        // 向下
                        if (Math.abs(endY) > Math.abs(this.beginY)) {
                            console.log('-- subIsScroll - 向下');
                            // this.setState({
                            //     subIsScroll: false
                            // })
                            // callBack && callBack(true)
                        // 向上    
                        }else {
                            console.log('-- subIsScroll -- 向上');
                            // this.setState({
                            //     subIsScroll: true
                            // })
                            // callBack && callBack(false)
                        }
                    }
                }}

                onPanResponderTerminate={ (event) => {
    
                    console.log('[ 另一个王八蛋被触发了 ] >',  event.nativeEvent.target)
                }}

                scrollEnabled={subIsScroll}
                numColumns={2}      // 一行3个
                maxHeight={500}      //此高度必须比第一次自动加载的撑起高度小
                data={ds}
                keyExtractor={(item, index) => {
                    return index.toString()
                }}
                renderItem={(item, index) => {
                    return this.renderItem(item, index);
                }}
                getItemLayout={(item, index) => {
                    return { length: 91, offset: 91 * index, index: index }
                }}
            />

        //     return (
        // <ScrollView
        // scrollEnabled={false}
        // >
        //     {this.getDatas().map((item, index) => {
        //         return this.renderItem(item, index);
        //     })}
        // </ScrollView>);
    }

    renderItem = (item, key) => {
        return <Text style={{ backgroundColor: '#fff' }}> {item.item} </Text>
    }


}


// const DataListView = (props) => {

//     const { subIsScroll } = props

//     // const [isRefrensh, setIsRefrensh] = useState(false)
//     const [isScroll, setScroll] = useState(true)

//     console.log('[ subIsScroll ] >', subIsScroll) 

//     const datas = () => {
//         let ds = [];
//         for (let i = 0; i < 100; i++) {
//             ds.push(`第${i}行`)
//         }
//         return ds
//     }

//     return <FlatList

//         // scrollEventThrottle={1}
//         // onScroll={(event) => {
//         //     console.log('[ event ] >', event.nativeEvent.contentOffset.y)
//         //     if (event.nativeEvent?.contentOffset.y <=1) {
//         //         setScroll(false)
//         //     }else{
//         //         setScroll(true)
//         //     }
//         // }}

//         scrollEnabled={ subIsScroll }
//         style={{ height: 300 }}
//         data={datas()}
//         keyExtractor={(item, index) => {
//             return index.toString()
//         }}
//         renderItem={(item, index) => {
//             return <RenderItem item={item.item} key={index}></RenderItem>
//         }}
//         getItemLayout={(item, index) => {
//             return { length: 91, offset: 91 * index, index: index }
//         }}
//     />

//     // return (
//     // <ScrollView
//     // scrollEnabled={false}
//     // >
//     //     {datas().map((item, index) => {
//     //         return <RenderItem item={item} key={index}></RenderItem>
//     //     })}
//     // </ScrollView>);

// }

// const RenderItem = (item, key) => {

//     return <Text style={{ backgroundColor: '#fff' }}> {item.item} </Text>

// }

// export default DataListView