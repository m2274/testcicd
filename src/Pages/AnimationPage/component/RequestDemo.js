import React, { useState, useEffect } from 'react'
import { ActivityIndicator, View, FlatList , Text} from 'react-native';

const RequestDemo = () => {

    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://reactnative.dev/movies.json')
          .then((response) => response.json())
          .then((json) => setData(json.movies))
          .catch((error) => console.error(error))
          .finally(() => setLoading(false));
      }, []);

    return (
        <View style={ { flex: 1, padding:24 } }>
            {isLoading ? <ActivityIndicator/> : 
            (<FlatList
            data={data}
            keyExtractor={ (item, index) => index.toString() }
            renderItem={ (item, index) => {
               return <Text> {item.title},{item.releaseYear} </Text>
            }}
            >
            </FlatList>) }
        </View>
    );
    
}

export default RequestDemo