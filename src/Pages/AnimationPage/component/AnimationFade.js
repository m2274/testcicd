import React , { useRef, useEffect, useState } from "react";
import { Animated, Easing, View, StyleSheet, PanResponder, Text } from 'react-native'

// // 渐变动画
// const AinmationFade = (props) => {

//     const fadeAinm = useRef(new Animated.Value(0)).current

//     useEffect(() => {
//         Animated.timing(fadeAinm,
//             {toValue:1,
//             duration:1000,
//             easing:Easing.back()
//         }).start();
//     },[fadeAinm]);

//     return <Animated.View style={{ ...props.style, opacity:fadeAinm}}>
//         {props?.children}
//     </Animated.View>
// }

// export default AinmationFade

// // 组合动画
// const AnimationGroup = () => {
//     const [name, setName ] = useState("歌德");
// }


// 拖动动画
const AinmationPan = (props) => {

    const pan = useRef(new Animated.ValueXY()).current

    const panResponder = useRef( PanResponder.create({
        onMoveShouldSetPanResponder:() => true,
        onPanResponderMove: Animated.event([null, { dx: pan.x, dy: pan.y }]),
        onPanResponderRelease: () => {
            Animated.spring(pan, { toValue: { x:0, y:0 }}).start();
        } 
    })).current

    return <Animated.View
    style={{          
        transform:[{translateX:pan.x},{ translateY: pan.y }]
    }}
    {...panResponder.panHandlers}
    >
        {props?.children}
    </Animated.View>

}

export default AinmationPan

