import React, { Component } from 'react';
import { View } from 'react-native';

export default class ComLineView extends Component {

    render() {
        return (<View style={[{ height: 0.5, backgroundColor: '#99A6BA' }, this.props.style]}></View>);
    }

} 
